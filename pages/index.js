import styles from "../styles/Home.module.css";
import axios from "axios";

const line = require("@line/bot-sdk");
export default function Home() {
  const onSendOTP = async () => {
    // let header = new Headers({
    //   "Access-Control-Allow-Origin": "*",
    //   "Content-Type": "multipart/form-data",
    // });
    // const res = await fetch("http://www.thsms.com/api/rest", {
    //   method: "GET",
    //   header,
    //   params: {
    //     method: "send",
    //     username: "tectony",
    //     password: "&1Va64vBHq5F99AJ",
    //     from: "VIP",
    //     to: "0933275947",
    //     message: "Your verification code is: ",
    //   },
    // });
    var OTPnumber = "";
    var possible = "0123456789";
    for (var i = 0; i < 6; i++) {
      OTPnumber += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    var Ref = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 6; i++) {
      Ref += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    var messages = "รหัสอ้างอิง : " + Ref + "\r\n" + "รหัส OTP : " + OTPnumber;
    var data = {
      otp: OTPnumber,
      ref: Ref,
      phone: "0933275947",
      messages: messages,
    };
    // var phone = await this.state.phonenumber;
    await axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/cors",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
    // axios({
    //   method: "get",
    //   url: "http://www.thsms.com/api/rest",
    //   headers: {
    //     "Access-Control-Allow-Origin": "*",
    //     "Access-Control-Allow-Methods":
    //       "GET, POST, OPTIONS, PUT, PATCH, DELETE",
    //     "Access-Control-Allow-Headers":
    //       "X-Requested-With,content-type,Authorization",
    //     "Access-Control-Allow-Credentials": true,
    //   },
    //   params: {
    //     method: "send",
    //     username: "tectony",
    //     password: "&1Va64vBHq5F99AJ",
    //     from: "VIP",
    //     to: "0933275947",
    //     message: "Your verification code is: ",
    //   },
    // }).then(
    //   (response) => {},
    //   (error) => {}
    // );
  };

  const onSendLineMess = async () => {
    const client = new line.Client({
      channelSecret: "9c77e8f81a1843005b93071ab5317819",
      channelAccessToken:
        "cQetw3YD09xOzfa/JyahOM/UazgSo2uZECWwgpbJr0KFIKVv+naDyLvNAJE+gydaCy2BpjxDeB48tZGrJcJzs1Un8e1OHBDOjnsF77iYU8n/K72XFfHLB9q714/HvcGiacA55rQlCnHEwVU3X+TmSAdB04t89/1O/w1cDnyilFU=",
    });
    const message = {
      type: "text",
      text: "Hello World!",
    };
    client
      .pushMessage("Uf08604dbfae9ffdc8123032a8cb2d26c", message)
      .then((res) => {
        console.log(res);
      });
  };

  return (
    <div className={styles.container}>
      <button onClick={onSendOTP}>ส่ง OTP</button>
      <button onClick={onSendLineMess}>ส่ง LINE message</button>
    </div>
  );
}
